<?php

/**
 * Implementation of hook_node_info().
 */
function homepage_slideshow_node_info() {
  $items = array(
    'homepage_slide' => array(
      'name' => t('Homepage Slide'),
      'module' => 'features',
      'description' => t('For the new homepage slider, 2015.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function homepage_slideshow_views_api() {
  return array(
    'api' => '2',
  );
}
